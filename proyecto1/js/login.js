// Modal - Login

const modalContainer = document.querySelector("#modal-container")
const abrirModal = document.querySelector("#modal-open")
const cerrarModal = document.querySelector("#cerrar-modal")
const logOut = document.querySelector("#cerrar-sesion")
const loginUsuario = document.querySelector("#modal-login")
const saludoUsuario = document.querySelector("#saludo-usuario")

abrirModal.addEventListener("click",()=>{
    modalContainer.classList.add("modal-container-active")
})

cerrarModal.addEventListener("click",()=>{
    modalContainer.classList.remove("modal-container-active")
})

let usuario = localStorage.getItem("usuario");

pedirUsuario = ()=>{
    usuario = document.querySelector("#user-name").value;
    localStorage.setItem("usuario",usuario);
}
if (!usuario){
    pedirUsuario();
}
document.querySelector("#saludo-usuario").innerText = `¡Bienvenidx ${usuario}!`;

loginUsuario.addEventListener("click",()=>{
  pedirUsuario();
  cargarUsuario();
  mostrarSaludo();
})

logOut.addEventListener("click",()=>{
    swal({
        title: "Estás seguro?",
        text: "Puedes volver a loguearte después.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          despedirUsuario();
        } else {
          swal("Que bueno que sigas acá!");
        }
      });
})
