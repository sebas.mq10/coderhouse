// Conversor celsius - Fahrenheit

const $ = document.querySelector.bind(document)

$("#boton-celsius").onclick = () =>{
    const inputcelsius = Number($("#gradoscelsius").value)
    
    if(!inputcelsius){
        swal("Ingresa un número por favor")
    }else{
        document.getElementById('resultado-conversion').innerHTML = (`Corresponde a ${celsiusAFahrenheit(inputcelsius).toFixed(0)} grados fahrenheit  `);
    }    
}

$("#boton-fahrenheit").onclick = () =>{
    const inputFahrenheit = Number($("#gradosFahrenheit").value)

    if(!inputFahrenheit){
        swal("Ingresa un número por favor")
    }else{
        document.getElementById('resultado-conversion').innerHTML = (`Corresponde a ${fahrenheitAcelsius(inputFahrenheit).toFixed(0)} grados celsius  `);
    }    
}

$("#reset-temperatura").onclick=()=>{
    document.getElementById('resultado-conversion').innerHTML = ("")
}

$("#reset-temp").onclick=()=>{
    document.getElementById('resultado-conversion').innerHTML = ("")
}

// Ejercicio con Arrays

$("#boton-arrays").onclick = () =>{
    arrayVacio = [];
    let resultadoArrays = $("#resultado-arrays");

    let numeroArrayUno = document.querySelector("#numeroArrayUno").value
    let numeroArrayDos= document.querySelector("#numeroArrayDos").value
    let numeroArrayTres = document.querySelector("#numeroArrayTres").value
    let numeroArrayCuatro = document.querySelector("#numeroArrayCuatro").value
    let numeroArrayCinco= document.querySelector("#numeroArrayCinco").value
    let numeroArraySeis = document.querySelector("#numeroArraySeis").value

    arrayVacio.push(numeroArrayUno,numeroArrayDos,numeroArrayTres,numeroArrayCuatro,numeroArrayCinco,numeroArraySeis);
    resultadoArrays.append(`Los nros positivos son : ${mostrarPositivos(arrayVacio)}  `);
    resultadoArrays.append(`Los nros negativos son : ${mostrarNegativos(arrayVacio)} `);
}

$("#reset-arrays").onclick = () =>{
    document.getElementById('resultado-arrays').innerHTML = ("")
}

// Ejercicio Nros Primos

$('#boton-primos').onclick = () =>{
    
    primerosCincuentaPrimos=()=>{
        const numerosPrimos = [];
        let numeroDePrueba = 2;
      
        while (numerosPrimos.length < 50) {
          if (esPrimo(numeroDePrueba) === true) {
            numerosPrimos.push(numeroDePrueba);
          }
          numeroDePrueba++;
        }
        return numerosPrimos;
      }
    document.querySelector("#resultado-primos").innerText=primerosCincuentaPrimos();
}

$("#reset-primos").onclick = () =>{
    document.getElementById('resultado-primos').innerHTML = ("")
}

// Calculadora

$("#boton-calculadora").onclick = () =>{
    let numero1 = $("#numero1").value
    let operador = $("#operador").value
    let numero2 = $("#numero2").value

    let resultadoCalculadora = $("#resultado-calculadora");

    calculadora=()=>{
        
        if(operador === "+"){
            resultadoCalculadora.innerHTML = ("Tu resultado es " + sumar(numero1,numero2))
        } else if (operador === "-"){
            resultadoCalculadora.innerHTML = ("Tu resultado es " + restar(numero1,numero2))
        }else if(operador === "/"){
            resultadoCalculadora.innerHTML = ("Tu resultado es " + dividir(numero1,numero2))
        }else if(operador === "*"){
            resultadoCalculadora.innerHTML = ("Tu resultado es " + multiplicar(numero1,numero2))
        }else{
            swal("Ingresaste algo mal");
        }
    }
        calculadora();
}


$("#resetea-calculadora").onclick = () =>{
    document.getElementById('resultado-calculadora').innerHTML = ("")
}

// Promedio de notas

$("#boton-siguiente").onclick = () =>{
    const cantidadParciales = Number(
      document.querySelector("#cantidad-parciales").value
    );

    if(!cantidadParciales){
      swal("No ingresaste parciales")  
    }else{
        borrarParciales();
        crearParciales(cantidadParciales);
    return false;
    }
};
  
$("#boton-calcular").onclick = ()=> {
    const numeros = obtenerNotasParciales();
    mostrarNota('mayor', calculaMayor(numeros));
    mostrarNota('menor', calculaMenor(numeros));
    mostrarNota('promedio', calculaPromedio(numeros));
    mostrarResultados()
};
   
$("#resetear").onclick = resetear=()=> {
    borrarParciales();
    ocultarResultados()
};
  

