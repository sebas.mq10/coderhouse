// Calculadora

sumar = (numero1,numero2)=>{
    return Number(numero1) + Number(numero2)
}

restar = (numero1,numero2)=>{
    return numero1 - numero2
}

dividir = (numero1,numero2)=>{
    return (numero1 / numero2).toFixed(2)
}

multiplicar = (numero1,numero2)=>{
    return numero1 * numero2
}

// Nros Primos

esPrimo = (n)=>{
    if (n < 2) return false;
    if (n == 2) return true;
    let numero = Math.sqrt(n);
    for (let i = 2; i <= numero; i++) {
      if (n % i == 0) {
        return false;
      }
    }
    return true;
  }


// celsius - Fahrenheit

celsiusAFahrenheit = (inputcelsius) =>{
  return ((inputcelsius * 1.8) + 32);
}

fahrenheitAcelsius = (inputFahrenheit) =>{
  return ((inputFahrenheit - 32) * (5 / 9));
}

// Ejercicio con Arrays

mostrarPositivos=(n) =>{
  let arrayPositivo = [];
  for (i = 0; i < n.length; i++) {
    if (n[i] >= 0) {
      arrayPositivo.push(n[i]);
    }
  }
  return arrayPositivo;
}

mostrarNegativos=(n) =>{
  let arrayNegativo = [];
  for (i = 0; i < n.length; i++) {
    if (n[i] <= 0) {
      arrayNegativo.push(n[i]);
    }
  }
  return arrayNegativo;
}

// Promedio de notas

calculaMayor=(numeros)=> {
  let mayorNumero = numeros[0];

  for (let i = 1; i < numeros.length; i++) {
    if (numeros[i] > mayorNumero)
      mayorNumero = numeros[i];
  }
  return mayorNumero;
}

calculaMenor=(numeros)=> {
  let menorNumero = numeros[0];
  for (let i = 1; i < numeros.length; i++) {
    if (numeros[i] < menorNumero)
      menorNumero = numeros[i];
  }
  return menorNumero;
}

calculaPromedio=(numeros)=> {
  let suma = 0;
  for (let i = 0; i < numeros.length; i++) 
    suma += numeros[i];
  let promedio = (suma / numeros.length).toFixed(2)
  return promedio
}

crearParcial=(indice)=> {
  let $div = document.createElement("div");
  $div.className = "parcial";
  
  const nuevosParciales = document.createElement("input");
  nuevosParciales.type = "number";
  const textoParciales = document.createTextNode(
    "Nota del parcial " + (indice + 1) + ":" + " "
    );
    $div.appendChild(textoParciales);
    $div.appendChild(nuevosParciales);
    const nodoNota = document.querySelector("#notas");
    nodoNota.appendChild($div);
  }
  
crearParciales=(cantidadParciales)=> {
  if (cantidadParciales > 0) {
    mostrarBotonCalculo();
  }
  for (let i = 0; i < cantidadParciales; i++) 
    crearParcial(i);
}

borrarParciales=()=> {
  const $notasParciales = document.querySelectorAll(".parcial");
  for (let i = 0; i < $notasParciales.length; i++) 
    $notasParciales[i].remove();
}

mostrarResultados=()=>{
  $('#resultados').className = '';
}

ocultarResultados=()=>{
  $('#resultados').className = 'oculto'
  $("#boton-calcular").className = 'oculto'
}

mostrarBotonCalculo=()=> {
  $("#boton-calcular").className = "btn btn-outline-light boton-sombra";
}

obtenerNotasParciales=()=> {
  let $notasTotales = document.querySelectorAll(".parcial input");
  let notas = [];

  for (let i = 0; i < $notasTotales.length; i++) {
    notas.push(Number($notasTotales[i].value));
  }
  return notas;
}

mostrarNota = (tipo, valor)=> {
  document.querySelector(`#${tipo}-nota`).textContent = valor
}

// Loader

cargarUsuario = () =>{
  const loader = document.querySelector("#loader")
  loader.classList.add("loader-active")
  modalContainer.classList.remove("modal-container-active")
  setTimeout (()=>
    loader.classList.remove("loader-active")
  ,1000)
}

mostrarSaludo = () =>{
  setTimeout(()=>
    document.querySelector("#saludo-usuario").innerText = `¡Bienvenidx ${usuario}!`
  ,1000)
}

despedirUsuario = ()=>{
  cargarUsuario()
  setTimeout(()=>
    swal("Deslogueado exitosamente!", {
      icon: "success",
    })
  ,1000)
  setTimeout(()=>
    localStorage.removeItem("usuario"),
    document.querySelector("#saludo-usuario").innerText = ``
  ,1000)
}