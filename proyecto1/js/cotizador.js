const $ = document.querySelector.bind(document)

pedirDataDolar = () =>{
    fetch('./data.json')
        .then(function(resp){
            return resp.json()
        })
        .then(function(data){
            convertir(data.divisas[0].oficial)
        })
}

pedirDataEuro = () =>{
    fetch('./data.json')
        .then((resp)=> resp.json())
        .then((data)=>{
            convertir(data.divisas[1].oficial)
        })
}

pedirDataLibra = () =>{
    fetch('./data.json')
        .then((resp)=> resp.json())
        .then((data)=>{
            convertir(data.divisas[2].oficial)
        })
}

pedirDataYen = () =>{
    fetch('./data.json')
        .then((resp)=> resp.json())
        .then((data)=>{
            convertir(data.divisas[3].oficial)
        })
}

pedirDataRupia = () =>{
    fetch('./data.json')
        .then((resp)=> resp.json())
        .then((data)=>{
            convertir(data.divisas[4].oficial)
        })
}

pedirDataRublo = () =>{
    fetch('./data.json')
        .then((resp)=> resp.json())
        .then((data)=>{
            convertir(data.divisas[5].oficial)
        })
}

convertir = (cotizacionMoneda) =>{
    let valores = parseInt(document.getElementById("valor").value);
    let resultado= 0;
    let moneda= cotizacionMoneda;
        resultado = moneda * valores;
        document.getElementById('resultado-cotizacion').innerHTML = "Corresponde a $" + resultado + " pesos";
}

$("#cotizador").onclick=()=>{
    if(document.getElementById("dolar").checked){
        pedirDataDolar()
    }else if(document.getElementById("euro").checked){
        pedirDataEuro()
    }else if(document.getElementById("libra").checked){
        pedirDataLibra()
    }else if(document.getElementById("yen").checked){
        pedirDataYen()
    }else if(document.getElementById("rupia").checked){
        pedirDataRupia()
    }else if(document.getElementById("rublo").checked){
        pedirDataRublo()
    }else{
        swal("Completa todos los requerimientos por favor")
    }
}

$("#reset-cotizador").onclick=()=>{
    document.getElementById('resultado-cotizacion').innerHTML = ("")
}

